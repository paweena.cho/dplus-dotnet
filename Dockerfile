FROM mcr.microsoft.com/dotnet/core/aspnet:3.0.1-alpine3.9

WORKDIR /usr/src/dplusapp

COPY ./bin/Release/netcoreapp3.0/publish . 
#path ของเรา

EXPOSE 80

ENTRYPOINT ["dotnet", "dplus-netcore.dll"]
#คำสั่ง run 